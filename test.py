import os
import numpy as np
import cv2 as cv
import tensorflow as tf
import datetime

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"


def unet_method(frame):
    timer = datetime.datetime.now()
    H, W, _ = frame.shape
    ori_frame = frame
    frame = cv.resize(frame, (128, 128))
    frame = np.expand_dims(frame, axis=0)
    frame = frame / 255.0

    mask = model.predict(frame)[0]
    mask = mask > 0.5
    mask = mask.astype(np.float32)
    mask = cv.resize(mask, (W, H))
    mask = np.expand_dims(mask, axis=-1)

    combine_frame = ori_frame * mask

    combine_frame = combine_frame.astype(np.uint8)
    print('operations takes : ', (datetime.datetime.now() - timer).total_seconds() * 1000, 'ms')
    return combine_frame


if __name__ == "__main__":
    video_path = 0  # "ME.mp4"
    model = tf.keras.models.load_model(
        "newModel_mean_squared_error_b24_e10_people_segmentation_old_no_valrecall_LR02.h5")
    cap = cv.VideoCapture(video_path)
    while cap.isOpened():
        ret, frame = cap.read()
        combine_frame = unet_method(frame)
        cv.imshow('Unet', combine_frame)
        key = cv.waitKey(1)
        if (key == 32 or key == 10):  # to exit press space or enter
            break
