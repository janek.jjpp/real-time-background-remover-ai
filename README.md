Real-Time background removal application made from scratch. 
The neural network model is a modified U-net model, with reduced structure resulitng in 31mln tranable parameters.
Model was trained with a custom training function, with binary-crossentropy as a loss function.
The project was developed in Python 3.9 with Tensorflow and Keras using the COCO and Kagle datasets.
