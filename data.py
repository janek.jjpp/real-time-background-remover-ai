import os
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
import numpy as np
import cv2
from glob2 import glob
from sklearn.model_selection import train_test_split
import tensorflow as tf


def load_data(data_path):
    images = sorted(glob(os.path.join(data_path, "images/*")))
    masks = sorted(glob(os.path.join(data_path, "masks/*")))

    train_x, test_x = train_test_split(images, test_size=0.2, random_state=42)
    train_y, test_y = train_test_split(masks, test_size=0.2, random_state=42)

    return (train_x, train_y), (test_x, test_y)


def read_image(path):
    x = cv2.imread(path, cv2.IMREAD_COLOR)
    # x = cv2.resize(x, (256, 256))
    x = cv2.resize(x, (128, 128))
    x = x / 255.0
    x = x.astype(np.float32)
    return x


def read_mask(path):
    x = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    # x = cv2.resize(x, (256, 256))
    x = cv2.resize(x, (128, 128))
    x = x.astype(np.float32)
    x = np.expand_dims(x, axis=-1)
    return x


def preprocess(image_path, mask_path):
    def f(image_path, mask_path):
        image_path = image_path.decode()
        mask_path = mask_path.decode()

        x = read_image(image_path)
        y = read_mask(mask_path)

        return x, y

    image, mask = tf.numpy_function(f, [image_path, mask_path], [tf.float32, tf.float32])
    image.set_shape([128, 128, 3])
    mask.set_shape([128, 128, 1])

    return image, mask


def tf_dataset(images, masks, batch=8):
    dataset = tf.data.Dataset.from_tensor_slices((images, masks))
    dataset = dataset.shuffle(buffer_size=5000)
    dataset = dataset.map(preprocess)
    dataset = dataset.batch(batch)
    dataset = dataset.prefetch(2)
    return dataset


data_path = "people_segmentation"
(train_x, train_y), (test_x, test_y) = load_data(data_path)
print(f"Training: x {len(train_x)}  -  y {len(train_y)}")
print(f"Testing: x {len(test_x)}  -  y {len(test_y)}")

train_dataset = tf_dataset(train_x, train_y, batch=12)

batch = 12
train_steps = len(train_x)//batch
if len(train_x) % batch != 0:
    train_steps += 1

print(train_steps)
