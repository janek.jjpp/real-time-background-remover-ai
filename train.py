import os
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
import numpy as np
import cv2
import tensorflow as tf
from model import build_unet
from data import load_data, tf_dataset
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, CSVLogger, EarlyStopping


import tensorflow.keras.backend as K
from typing import Callable

def binary_tversky_coef(y_true: tf.Tensor, y_pred: tf.Tensor, beta: float, smooth: float = 1.) -> tf.Tensor:
    axis_to_reduce = range(1, K.ndim(y_pred))  # All axis but first (batch)
    numerator = K.sum(y_true * y_pred, axis=axis_to_reduce)  # p*p̂
    denominator = y_true * y_pred + beta * (1 - y_true) * y_pred + (1 - beta) * y_true * (
            1 - y_pred)  # p*p̂ + β*(1-p)*p̂ + (1-β)*p*(1-p̂)
    denominator = K.sum(denominator, axis=axis_to_reduce)

    return (numerator + smooth) / (
            denominator + smooth)  # (p*p̂ + smooth)/[p*p̂ + β*(1-p)*p̂ + (1-β)*p*(1-p̂) + smooth]


def convert_to_logits(y_pred: tf.Tensor) -> tf.Tensor:
    y_pred = K.clip(y_pred, tf.keras.backend.epsilon(), 1 - tf.keras.backend.epsilon())

    return K.log(y_pred / (1 - y_pred))


def binary_dice_coef_loss(smooth: float = 1.) -> Callable[[tf.Tensor, tf.Tensor], tf.Tensor]:
    def loss(y_true: tf.Tensor, y_pred: tf.Tensor) -> tf.Tensor:
        return 1 - binary_tversky_coef(y_true=y_true, y_pred=y_pred, beta=0.5, smooth=smooth)

    return loss


def binary_weighted_dice_crossentropy_loss(smooth: float = 1.,
                                           beta: float = 0.5) -> Callable[[tf.Tensor, tf.Tensor], tf.Tensor]:
    assert 0. <= beta <= 1., "Loss weight has to be between 0.0 and 1.0"

    def loss(y_true: tf.Tensor, y_pred: tf.Tensor) -> tf.Tensor:
        cross_entropy = K.binary_crossentropy(target=y_true, output=y_true)
        axis_to_reduce = range(1, K.ndim(cross_entropy))
        cross_entropy = K.mean(x=cross_entropy, axis=axis_to_reduce)
        dice_coefficient = binary_tversky_coef(y_true=y_true, y_pred=y_pred, beta=0.5, smooth=smooth)
        return beta * (1. - dice_coefficient) + (1. - beta) * cross_entropy

    return loss


def binary_tversky_loss(beta: float) -> Callable[[tf.Tensor, tf.Tensor], tf.Tensor]:
    def loss(y_true: tf.Tensor, y_pred: tf.Tensor) -> tf.Tensor:
        return 1 - binary_tversky_coef(y_true, y_pred, beta=beta)

    return loss


def binary_weighted_cross_entropy(beta: float, is_logits: bool = False) -> Callable[[tf.Tensor, tf.Tensor], tf.Tensor]:
    def loss(y_true: tf.Tensor, y_pred: tf.Tensor) -> tf.Tensor:
        if not is_logits:
            y_pred = convert_to_logits(y_pred)
        wce_loss = tf.nn.weighted_cross_entropy_with_logits(labels=y_true, logits=y_pred, pos_weight=beta)
        axis_to_reduce = range(1, K.ndim(wce_loss))
        wce_loss = K.mean(wce_loss, axis=axis_to_reduce)
        return wce_loss

    return loss


def binary_balanced_cross_entropy(beta: float, is_logits: bool = False) -> Callable[[tf.Tensor, tf.Tensor], tf.Tensor]:
    if beta == 1.:  # To avoid division by zero
        beta -= tf.keras.backend.epsilon()

    def loss(y_true: tf.Tensor, y_pred: tf.Tensor) -> tf.Tensor:
        if not is_logits:
            y_pred = convert_to_logits(y_pred)
        pos_weight = beta / (1 - beta)
        bce_loss = tf.nn.weighted_cross_entropy_with_logits(labels=y_true, logits=y_pred, pos_weight=pos_weight)
        bce_loss = bce_loss * (1 - beta)
        axis_to_reduce = range(1, K.ndim(bce_loss))
        bce_loss = K.mean(bce_loss, axis=axis_to_reduce)

        return bce_loss

    return loss


def binary_focal_loss(beta: float, gamma: float = 2.) -> Callable[[tf.Tensor, tf.Tensor], tf.Tensor]:
    def loss(y_true: tf.Tensor, y_pred: tf.Tensor) -> tf.Tensor:
        f_loss = beta * (1 - y_pred) ** gamma * y_true * K.log(y_pred)  # β*(1-p̂)ᵞ*p*log(p̂)
        f_loss += (1 - beta) * y_pred ** gamma * (1 - y_true) * K.log(1 - y_pred)  # (1-β)*p̂ᵞ*(1−p)*log(1−p̂)
        f_loss = -f_loss  # −[β*(1-p̂)ᵞ*p*log(p̂) + (1-β)*p̂ᵞ*(1−p)*log(1−p̂)]
        axis_to_reduce = range(1, K.ndim(f_loss))
        f_loss = K.mean(f_loss, axis=axis_to_reduce)
        return f_loss

    return loss

"""training parameters"""
data_path = "people_segmentation"
input_shape = (128, 128, 3)
batch_size = 32
epoch = 20
# lr = 1e-4
lr = 0.00005
model_path = "newModel_mean_squared_error_b24_e10_people_segmentation_old_no_valrecall_LR02.h5"
csv_path = "newModel_mean_squared_error_b24_e10_people_segmentation_old_no_valrecall_LR02.csv"

(train_x, train_y), (test_x, test_y) = load_data(data_path)
print(f"Training: x {len(train_x)}  -  y {len(train_y)}")
print(f"Testing: x {len(test_x)}  -  y {len(test_y)}")

train_dataset = tf_dataset(train_x, train_y, batch=batch_size)
valid_dataset = tf_dataset(test_x, test_y, batch=batch_size)

model = build_unet(input_shape)
model.compile(
    loss="binary_crossentropy",
    optimizer=tf.optimizers.Adam(lr),
    metrics=[
        tf.keras.metrics.MeanIoU(num_classes=2),
        tf.keras.metrics.Recall(),
        tf.keras.metrics.Precision()
    ]
)

callbacks = [
    ModelCheckpoint(model_path, monitor="val_loss", verbose=1),
    ReduceLROnPlateau(monitor="val_loss", patience=5, factor=0.1, verbose=1),
    CSVLogger(csv_path),
    EarlyStopping(monitor="val_loss", patience=10)
]

train_steps = len(train_x) // batch_size
if len(train_x) % batch_size != 0:
    train_steps += 1

valid_steps = len(test_x) // batch_size
if len(test_x) % batch_size != 0:
    valid_steps += 1

model.fit(
    train_dataset,
    validation_data=valid_dataset,
    epochs=epoch,
    steps_per_epoch=train_steps,
    validation_steps=valid_steps,
    callbacks=callbacks
)
